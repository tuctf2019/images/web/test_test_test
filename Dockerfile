
FROM php:7.2-apache
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
COPY ./apache2.conf /etc/apache2/apache2.conf
COPY ./src/ /var/www/html/
