# Test Test Test -- Web -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/web/test_test_test)

## Chal Info

Desc: `There are so many tests going on right now, why don't take a deep breath and list them out before you forget one?`

Hints:

* Pages moving too fast? Look for a way to sloooow them down

Flag: `TUCTF{d0nt_l34v3_y0ur_d1r3ct0ry_h4n61n6}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/test_test_test)

Ports: 80

Example usage:

```bash
docker run -d -p 127.0.0.1:8080:80 asciioverflow/test_test_test:tuctf2019
```
